/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package TestService;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author Name
 */
public class TestCustomerService {
    public static void main(String[] arge){
        CustomerService cs = new CustomerService();
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0885666667"));
        Customer cus1 = new Customer("kob","0885669778");
        cs.addNew(cus1);
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
        Customer delCus = cs.getByTel("0885669778");
        delCus.setTel("0885669777");
        cs.update(delCus);
        System.out.println("After Updated");
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
        cs.delete((delCus));
        for(Customer customer : cs.getCustomers()){
            System.out.println(customer);
        }
    }
}
